extern crate kiss3d;
extern crate conc_voxels;
extern crate nalgebra as na;

use kiss3d::{
    scene::SceneNode,
    window::{State, Window},
    camera::{Camera, FirstPerson},
    planar_camera::PlanarCamera,
    renderer::Renderer,
    post_processing::PostProcessingEffect
};
use na::Vector3;
use rgb::RGBA;

use std::default::Default;

use conc_voxels::{
    chunk::{
        Chunk, CHUNK_SIZE,
        generator::{ChunkGenerator, RandomChunkGenerator},
        data::ReadChunkData, pos::InnerPos
    },
    block::{
        BlockIdx, TileMetadata, BlockRef,
        SimpleCube,
        palette::{Palette, PalettePair}
    },
    utils::Faces
};

struct AppState {
    pub blocks : [[[Option<SceneNode> ; CHUNK_SIZE] ; CHUNK_SIZE] ; CHUNK_SIZE],
    pub camera : FirstPerson
}

impl State for AppState {
    fn step(&mut self, _ : &mut Window) {}
    fn cameras_and_effect_and_renderer(&mut self) -> (
        Option<&mut Camera>,
        Option<&mut PlanarCamera>,
        Option<&mut Renderer>,
        Option<&mut PostProcessingEffect>,
    ) {
        (Some(&mut self.camera), None, None, None)
    }
}

fn display_chunk(chunk : &Chunk, window : &mut Window)
-> [[[Option<SceneNode> ; CHUNK_SIZE] ; CHUNK_SIZE] ; CHUNK_SIZE] {
    let mut res : [[[Option<SceneNode> ; CHUNK_SIZE] ; CHUNK_SIZE] ; CHUNK_SIZE] =
        Default::default();
    for pos in InnerPos::all() {
        let block = chunk.get(pos);
        // Only display visible blocks
        if chunk.get_covered(pos) != Faces::all() {
            if let Some(color) = chunk.palettes().display_color(block) {
                let mut displayed_block = window.add_cube(1.0, 1.0, 1.0);
                displayed_block.set_local_translation(Vector3::new(
                    pos.x(), pos.y(), pos.z()
                ).into());
                let as_float : RGBA<f32> = color.into();
                displayed_block.set_color(
                    as_float.r / 255.0, as_float.g / 255.0, as_float.b / 255.0
                );
                res[pos.x::<usize>()][pos.y::<usize>()][pos.z::<usize>()] = Some(displayed_block);
            }
        }
    }
    res
}

fn main() {
    let mut window = Window::new("Kiss3d: cube");
    let solid = BlockRef::leak(SimpleCube::from(("cube", (255, 150, 200).into())));
    let palettes = PalettePair { main : Palette::null(), custom : Palette::Static(solid) };
    let invisible = BlockIdx::null();
    let visible = BlockIdx::from_custom(0, &palettes);
    let generator = RandomChunkGenerator::new(
        (visible, TileMetadata::null()).into(),
        (invisible, TileMetadata::null()).into(),
        0.3
    );
    let mut rng = rand::thread_rng();
    let chunk = generator.generate(&mut rng, palettes);
    let mut camera = FirstPerson::new(
        Vector3::new(0.0, 0.0, 0.0).into(),
        Vector3::new(10.0, 10.0, 10.0).into());
    camera.set_move_step(0.05);
    let state = AppState {
        blocks : display_chunk(&chunk, &mut window),
        camera : camera
    };

    window.render_loop(state);
}
