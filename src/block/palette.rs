use derive_more::From;
use std::ops::Index;
use num::ToPrimitive;
use arrayvec::ArrayVec;
use rgb::RGBA8;

use std::sync::Arc;

use super::{BlockIdx, BlockRef, NULL_BLOCK, BlockTy, BlockData};
use crate::chunk::CHUNK_BLOCKS;


/// A page of blocks.
#[derive(Clone)]
pub struct BlockPage {
    blocks : ArrayVec<[Arc<BlockTy>; CHUNK_BLOCKS]>
}

impl<T: ToPrimitive> Index<T> for BlockPage {
    type Output = BlockTy;

    fn index(&self, idx : T) -> &BlockTy {
        let idx = if let Some(u) = idx.to_usize() { u } else { return &NULL_BLOCK };
        self.blocks.get(idx).map(|a| a.as_ref()).unwrap_or(&NULL_BLOCK)
    }
}

/// The palette of a chunk, which is either:
/// - Just a main block (e.g. air)
/// - Stored in the chunk as a Vec
/// - Referenced as an Arc
#[derive(Clone, From)]
pub enum Palette {
    Static(BlockRef<'static>),
    Shared(Arc<BlockPage>)
}

impl Palette {
    pub fn null() -> Palette {
        BlockRef::Borrowed(&NULL_BLOCK).into()
    }
}

impl<T: ToPrimitive> Index<T> for Palette {
    type Output = BlockTy;

    fn index(&self, idx : T) -> &BlockTy {
        let idx = if let Some(u) = idx.to_usize() { u } else { return &NULL_BLOCK };
        match self {
            Palette::Static(id) => if idx == 0 { id.as_ref() } else { &NULL_BLOCK },
            Palette::Shared(a) => &a[idx]
        }
    }
}

/// A palette pair, which each chunk carries to identify its blocks
#[derive(Clone, From)]
pub struct PalettePair {
    /// The main palette of the chunk (e.g., a set of blocks common in a biome)
    pub main : Palette,
    /// The custom palette of the chunk (e.g., a small set of blocks found only in the chunk)
    pub custom : Palette
}

impl PalettePair {
    /// Get the overall display color for this block, if any
    pub fn display_color(&self, block : BlockData) -> Option<RGBA8> {
        if block.idx.get_metadata().visible() {
            Some(self[block.idx].color(block.metadata))
        } else {
            None
        }
    }
}

impl Index<BlockIdx> for PalettePair {
    type Output = BlockTy;

    fn index(&self, idx : BlockIdx) -> &BlockTy {
        let p_idx = idx.get_idx();
        if idx.get_metadata().custom() {
            &self.custom[p_idx]
        } else {
            &self.main[p_idx]
        }
    }
}
