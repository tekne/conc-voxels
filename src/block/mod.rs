use rgb::{RGB8, RGBA8};
use derive_more::From;

use std::sync::Arc;
use std::borrow::Cow;

use crate::utils::{Faces};

pub mod palette;
mod idx;
pub use idx::*;

/// An abstract block
pub trait Block {
    /// Get which sides of this block are *always* opaque
    fn always_opaque_sides(&self) -> Faces;
    /// Get which sides of this block are *always* solid
    fn always_solid_sides(&self) -> Faces;
    /// Get which sides of this block are opaque, given associated metadata
    #[allow(unused_variables)]
    fn opaque_sides(&self, metadata : TileMetadata) -> Faces {
        self.always_opaque_sides()
    }
    /// Get which sides of this block are solid, given associated metadata
    #[allow(unused_variables)]
    fn solid_sides(&self, metadata : TileMetadata) -> Faces {
        self.always_solid_sides()
    }
    /// Get whether this block is *always* visible
    fn always_visible(&self) -> bool;
    /// Get whether this block is visible, given associated metadata
    #[allow(unused_variables)]
    fn visible(&self, metadata : TileMetadata) -> bool {
        self.always_visible()
    }
    /// Get the RGBA color of this block if doing solid rendering. If the block has no color, i.e.
    /// is invisible, the return value is undefined.
    fn color(&self, metadata : TileMetadata) -> RGBA8;
    /// Get the name of this block, if it has any
    fn name(&self) -> Option<&str>;
    /// Get the appropriate flags for any index of this block, even without tile metadata
    fn get_arb_flags(&self) -> BlockFlags {
        use BlockFlag::*;
        let mut res = BlockFlags::empty();
        if self.always_visible() {
            res |= Visible;
        }
        if self.always_opaque_sides() == Faces::all() {
            res |= Opaque;
        }
        if self.always_solid_sides() == Faces::all() {
            res |= Solid
        }
        res
    }
    /// Get the appropriate flags for an index of this block, given tile metadata
    fn get_flags(&self, metadata : TileMetadata) -> BlockFlags {
        use BlockFlag::*;
        let mut res = BlockFlags::empty();
        if self.visible(metadata) {
            res |= Visible;
        }
        if self.opaque_sides(metadata) == Faces::all() {
            res |= Opaque;
        }
        if self.solid_sides(metadata) == Faces::all() {
            res |= Solid
        }
        res
    }
}

pub const BLANK_COLOR : RGBA8 = RGBA8 { r : 0, g : 0, b : 0, a : 0 };
pub static NULL_BLOCK : NullBlock = NullBlock {};

/// A solid cube. Has a color
#[derive(Debug, Clone)]
pub struct SimpleCube {
    color : RGB8,
    name : Option<Cow<'static, str>>
}

impl SimpleCube {
    pub const WHITE_COLOR : RGB8 = RGB8{ r : std::u8::MAX, g : std::u8::MAX, b : std::u8::MAX };

    /// Create a simple cube with no name or texture (just plain white)
    pub fn blank() -> SimpleCube {
        Self::with_color(Self::WHITE_COLOR)
    }
    /// Create a simple cube with a given name and no texture (just plain white)
    pub fn with_name<T: Into<Cow<'static, str>>>(name : T) -> SimpleCube {
        SimpleCube{ color : Self::WHITE_COLOR, name : Some(name.into()) }
    }
    /// Create a simple cube with no texture, just a color.
    pub fn with_color(color : RGB8) -> SimpleCube {
        SimpleCube{ color : color, name : None }
    }
    /// Rename this cube
    pub fn rename<T: Into<Cow<'static, str>>>(&mut self, name : T) {
        self.name = Some(name.into());
    }
}

impl<T: Into<Cow<'static, str>>> From<(T, RGB8)> for SimpleCube {
    /// Create a simple cube with the given name and color, but no texture
    fn from(t: (T, RGB8)) -> SimpleCube {
        SimpleCube { color : t.1, name : Some(t.0.into()) }
    }
}

impl Block for SimpleCube {
    fn always_opaque_sides(&self) -> Faces { Faces::all() }
    fn always_solid_sides(&self) -> Faces { Faces::all() }
    fn always_visible(&self) -> bool { true }
    fn color(&self, _ : TileMetadata) -> RGBA8 { self.color.alpha(std::u8::MAX) }
    fn name(&self) -> Option<&str> { self.name.as_ref().map(|c| c.as_ref()) }
}

/// An invisible, non-solid block.
#[derive(Debug, Clone)]
pub struct InvisibleGas {
    pub(self) name : Option<Cow<'static, str>>
}

impl InvisibleGas {
    /// Create a nameless gas
    pub fn new() -> InvisibleGas { InvisibleGas{ name : None } }
    /// Create a gas with the given name
    pub fn with_name<T: Into<Cow<'static, str>>>(name : T) -> InvisibleGas {
        InvisibleGas{ name : Some(name.into()) }
    }
}

impl Block for InvisibleGas {
    fn always_opaque_sides(&self) -> Faces { Faces::empty() }
    fn always_solid_sides(&self) -> Faces { Faces::empty() }
    fn always_visible(&self) -> bool { false }
    fn color(&self, _ : TileMetadata) -> RGBA8 { BLANK_COLOR }
    fn name(&self) -> Option<&str> { self.name.as_ref().map(|c| c.as_ref()) }
}

/// The type to store in containers holding blocks.
pub type BlockTy = dyn Block + 'static;

/// A reference to an abstract block, which may be an Arc.
#[derive(Clone, From)]
pub enum BlockRef<'a> {
    Borrowed(&'a BlockTy),
    Shared(Arc<dyn Block>)
}

impl<'a> BlockRef<'a> {
    pub fn leak<B: Block + 'static>(block : B) -> BlockRef<'a> {
        BlockRef::Borrowed(Box::leak(Box::new(block)))
    }
}

impl<'a> AsRef<dyn Block> for BlockRef<'a> {
    fn as_ref(&self) -> &(dyn Block + 'static) {
        match self {
            BlockRef::Borrowed(b) => *b,
            BlockRef::Shared(s) => s.as_ref()
        }
    }
}

/// The null block
#[derive(Debug, Copy, Clone)]
pub struct NullBlock {}

impl Block for NullBlock {
    fn always_opaque_sides(&self) -> Faces { Faces::empty() }
    fn always_solid_sides(&self) -> Faces { Faces::empty() }
    fn always_visible(&self) -> bool { false }
    fn color(&self, _ : TileMetadata) -> RGBA8 { BLANK_COLOR }
    fn name(&self) -> Option<&str> { Some("null") }
}
