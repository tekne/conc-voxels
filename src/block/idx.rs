use enumset::{EnumSet, EnumSetType};
use num::ToPrimitive;

use super::palette::PalettePair;

const METADATA_BITS : u16 = 7;
const METADATA_BITS_SET : u16 = 0b1111111;
const FLAG_BITS_SET : u8 = 0b1111;

/// Metadata flags for a block, to avoid having to consult the page to query simple info.
#[derive(Debug, EnumSetType)]
pub enum BlockFlag {
    /// This block is visible, so needs to be rendered if it can be seen
    Visible,
    /// This block is opaque, so blocks behind it cannot be seen
    Opaque,
    /// This block generates a solid collider in the physics engine
    Solid,
    /// This block is custom to this chunk (i.e. not part of the palette)
    Custom
    // OTHER BITS RESERVED
}

pub type BlockFlags = EnumSet<BlockFlag>;

/// Metadata for a block, including a set of flags. Guaranteed to fit in seven bits.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct BlockMetadata {
    pub(self) data : u8
}

impl BlockMetadata {
    /// Get null metadata (all flags off)
    pub fn null() -> BlockMetadata { BlockMetadata { data : 0 } }
    /// Get the flags set for this block
    #[inline]
    pub fn get_flags(&self) -> BlockFlags {
        BlockFlags::from_bits((self.data & FLAG_BITS_SET) as u128)
    }
    /// Get whether this block is custom to it's chunk
    #[inline]
    pub fn custom(&self) -> bool {
        self.get_flags().contains(BlockFlag::Custom)
    }
    /// Get whether this block is opaque
    #[inline]
    pub fn opaque(&self) -> bool {
        self.get_flags().contains(BlockFlag::Opaque)
    }
    /// Get whether this block is solid
    #[inline]
    pub fn solid(&self) -> bool {
        self.get_flags().contains(BlockFlag::Solid)
    }
    #[inline]
    pub fn visible(&self) -> bool {
        self.get_flags().contains(BlockFlag::Visible)
    }
}

impl From<BlockFlags> for BlockMetadata {
    /// Convert a set of flags into block metadata indicating they are set
    #[inline]
    fn from(flags : BlockFlags) -> BlockMetadata {
        BlockMetadata { data : flags.to_bits() as u8 }
    }
}

/// Metadata associated with an individual tile (rather than with an index).
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct TileMetadata {
    #[allow(dead_code)]
    data : u32
}

impl TileMetadata {
    pub fn null() -> TileMetadata { TileMetadata { data : 0 } }
}

/// An index into a page of blocks, plus some metadata
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct BlockIdx {
    data : u16
}

impl BlockIdx {
    /// Create a null index
    pub fn null() -> BlockIdx {
        BlockIdx::with_meta(0, BlockMetadata::null())
    }
    /// Check whether this index is null
    pub fn is_null(&self) -> bool {
        *self == Self::null()
    }
    /// Create a new index into a page of blocks given metadata. Panics if idx is out of range.
    pub fn with_meta<I: ToPrimitive, T: Into<BlockMetadata>>(idx : I, meta : T) -> BlockIdx {
        let idx = idx.to_u16().unwrap();
        let meta : BlockMetadata = meta.into();
        BlockIdx {
            data : (idx << METADATA_BITS) | (meta.data as u16)
        }
    }
    /// Make an index into the main palette, without metadata
    pub fn from_main<I: ToPrimitive>(idx : I, palettes : &PalettePair) -> BlockIdx {
        let idx = idx.to_u16().unwrap();
        Self::with_meta(idx, palettes.main[idx].get_arb_flags())
    }
    /// Make an index into the custom palette, without metadata
    pub fn from_custom<I: ToPrimitive>(idx : I, palettes : &PalettePair) -> BlockIdx {
        use BlockFlag::*;
        let idx = idx.to_u16().unwrap();
        Self::with_meta(idx, Custom | palettes.custom[idx].get_arb_flags())
    }
    /// Make an index into the main palette, with metadata
    pub fn from_main_with<I: ToPrimitive, M: Into<TileMetadata>>
    (idx : I, metadata : M, palettes : &PalettePair) -> BlockIdx {
        let idx = idx.to_u16().unwrap();
        Self::with_meta(idx, palettes.main[idx].get_flags(metadata.into()))
    }
    /// Make an index into the custom palette, with metadata
    pub fn from_custom_with<I: ToPrimitive, M: Into<TileMetadata>>
    (idx : I, metadata : M, palettes : &PalettePair) -> BlockIdx {
        use BlockFlag::*;
        let idx = idx.to_u16().unwrap();
        Self::with_meta(idx, Custom | palettes.custom[idx].get_flags(metadata.into()))
    }
    /// Get the associated index into the block page
    #[inline]
    pub fn get_idx(&self) -> usize {
        (self.data >> METADATA_BITS) as usize
    }
    /// Get this block's metadata
    #[inline]
    pub fn get_metadata(&self) -> BlockMetadata {
        BlockMetadata { data : (self.data & METADATA_BITS_SET) as u8 }
    }
}

/// Data associated with an individual block in a chunk.
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct BlockData {
    /// An index into this chunk's palettes, plus some associated metadata (to the index)
    pub idx : BlockIdx,
    /// Uninterpreted metadata for this block, to be interpreted as dictated by the index.
    pub metadata : TileMetadata
}

impl BlockData {
    pub fn null() -> BlockData {
        BlockData {
            idx : BlockIdx::null(),
            metadata : TileMetadata::null()
        }
    }
    /// Get the block metadata associated with this BlockData's internal BlockIdx
    pub fn block_meta(&self) -> BlockMetadata {
        self.idx.get_metadata()
    }
}

#[cfg(test)]
#[test]
fn block_idx_splits_properly() {
    let metadata = (BlockFlag::Visible | BlockFlag::Opaque | BlockFlag::Solid).into();
    let idx = BlockIdx::with_meta(12, metadata);
    assert_eq!(idx.get_idx(), 12);
    assert_eq!(idx.get_metadata(), metadata);
}
