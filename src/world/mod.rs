use parking_lot::RwLock;

pub mod generator;
use generator::WorldGenerator;

#[derive(Debug)]
pub struct World<G: WorldGenerator> {
    /// The chunks in this world
    pub chunks : ChunkMap,
    /// The regions of this world, which are clumps of chunks.
    /// Regions are stored together in files, and have similar physical
    /// properties to the generator (allowing medium scale continuity)
    pub regions : RegionMap,
    /// The hyper-region registry, which the world generator reads for
    /// large-scale contiguity, and simulators can query for data, e.g.
    /// area temperature / rain probability / etc.
    pub hyper_regions : RwLock<G::HyperRegions>,
    /// The generator for this world, which should be concurrent.
    pub generator : G
}

/// A concurrent map of chunk positions to chunks
#[derive(Debug, Clone)]
pub struct ChunkMap {
    //TODO: this
}

/// A concurrent map of region positions to regions
#[derive(Debug, Clone)]
pub struct RegionMap {
    //TODO: this
}
