use rand::Rng;
use derive_more::{From, Into};

use crate::block::{BlockIdx, palette::PalettePair, TileMetadata};
use super::{Chunk, pos::InnerPos};
use super::data::{raw::RawData, compressed::CompressedData, WriteChunkData};

pub trait ChunkGenerator {
    type InputData;
    fn generate(&self, data: Self::InputData, palettes : PalettePair) -> Chunk;
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
#[derive(From, Into)]
pub struct FullChunkGenerator {
    pub idx : BlockIdx,
    pub metadata : TileMetadata
}

impl ChunkGenerator for FullChunkGenerator {
    type InputData = ();
    fn generate(&self, _ : (), palettes : PalettePair) -> Chunk {
        Chunk::new(
            CompressedData::solid(self.idx, self.metadata),
            palettes
        )
    }
}

pub struct RandomChunkGenerator<'a, G : Rng> {
    pub foreground : FullChunkGenerator,
    pub background : FullChunkGenerator,
    pub proportion : f64,
    generator : std::marker::PhantomData<fn() -> &'a G>
}

impl<'a, G: Rng> RandomChunkGenerator<'a, G> {
    pub fn new(
        foreground : FullChunkGenerator,
        background : FullChunkGenerator,
        proportion : f64
    )
    -> RandomChunkGenerator<'a, G> {
        RandomChunkGenerator {
            foreground, background, proportion,
            generator : std::marker::PhantomData
        }
    }
}

impl<'a, G: Rng> ChunkGenerator for RandomChunkGenerator<'a, G> {
    type InputData = &'a mut G;
    fn generate(&self, gen : &'a mut G, palettes : PalettePair) -> Chunk {
        let mut data = Box::new(RawData::nulls());
        for pos in InnerPos::all() {
            let (target_idx, target_metadata) =
                if gen.gen_bool(self.proportion) {self.foreground.into()}
                else {self.background.into()};
            data.set_idx(pos, target_idx);
            data.set_meta(pos, target_metadata);
        }
        Chunk::new(data, palettes)
    }
}
