use crate::block::{palette::PalettePair, BlockIdx, TileMetadata, BlockData};

/// The length of one side of a chunk
pub const CHUNK_SIZE : usize = 8;
/// The amount of blocks contained in one chunk
pub const CHUNK_BLOCKS : usize = CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE;

pub mod pos;
use pos::{ChunkPos, InnerPos};
pub mod data;
use data::{ChunkDataEnum, ReadChunkData};
pub mod generator;

/// A cubic chunk of blocks
#[derive(Clone)]
pub struct Chunk {
    /// The modification version of this chunk
    version : u64,
    /// The palette pair defining the mapping between block indices and block IDs for this chunk
    palettes : PalettePair,
    /// This chunk's data
    data : ChunkDataEnum
}

impl Chunk {
    pub fn new<T: Into<ChunkDataEnum>>(data : T, palettes : PalettePair) -> Chunk {
        let cde : ChunkDataEnum = data.into();
        Chunk {
            version : 0,
            palettes : palettes,
            data : cde
        }
    }
    pub fn palettes(&self) -> &PalettePair { &self.palettes }
}

impl ReadChunkData for Chunk {
    fn get(&self, pos : InnerPos) -> BlockData {
        self.data.get(pos)
    }
    fn get_idx(&self, pos : InnerPos) -> BlockIdx {
        self.data.get_idx(pos)
    }
    fn get_meta(&self, pos : InnerPos) -> TileMetadata {
        self.data.get_meta(pos)
    }
}

/// A rectangle of chunks. Useful for small objects made out of chunks. Is a DST.
pub struct ChunkRectangle {
    x : u16, y : u16, z : u16,
    chunks : [Chunk]
}

impl ChunkRectangle {
    pub fn size(&self) -> usize {
        self.x as usize * self.y as usize * self.z as usize
    }
    pub fn chunks(&self) -> &[Chunk] { &self.chunks }
    pub fn idx(&self, pos : ChunkPos) -> Option<usize> {
        if pos[0] < 0 || pos[1] < 0 || pos[2] < 0 {
            return None;
        }
        let x = pos[0] as usize;
        let y = pos[1] as usize;
        let z = pos[2] as usize;
        let idx = x + self.x as usize * y + (self.x as usize * self.y as usize) * z;
        if idx < self.size() {
            Some(idx)
        } else {
            None
        }
    }
    pub fn get(&self, pos : ChunkPos) -> Option<&Chunk> {
        self.idx(pos).map(move |idx| &self.chunks[idx])
    }
    pub fn get_mut(&mut self, pos : ChunkPos) -> Option<&mut Chunk> {
        self.idx(pos).map(move |idx| &mut self.chunks[idx])
    }
}
