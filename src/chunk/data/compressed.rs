use rle_vec::RleVec;

use std::ops::Index;

use crate::block::{BlockIdx, TileMetadata};
use crate::chunk::{pos::InnerPos, CHUNK_BLOCKS};
use super::raw::RawData;
use super::{ReadChunkData, WriteChunkData};

/// Compressed chunk data stored as a set of containers
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct CompressedData<I = RleVec<BlockIdx>, M = RleVec<TileMetadata>> {
    /// For each block, an index into this chunk's palettes
    indices : I,
    /// Uninterpreted metadata for each block, to be interpreted as dictated by the index.
    metadata : M
}

impl CompressedData<RleVec<BlockIdx>, RleVec<TileMetadata>> {
    /// Try to make compressed data for a chunk out of a set of RleVecs
    pub fn new(
        indices : RleVec<BlockIdx>,
        metadata : RleVec<TileMetadata>
    )
    -> Result<CompressedData, (RleVec<BlockIdx>, RleVec<TileMetadata>)> {
        if indices.len() >= CHUNK_BLOCKS
            && metadata.len() >= CHUNK_BLOCKS {
            Ok(CompressedData{ indices, metadata })
        } else {
            Err((indices, metadata))
        }
    }
    /// The simplest case of compressed data: just repeat a single block index and metadata
    pub fn solid(idx : BlockIdx, meta : TileMetadata) -> CompressedData {
        let mut indices = RleVec::with_capacity(1);
        indices.push_n(CHUNK_BLOCKS, idx);
        let mut metadata = RleVec::with_capacity(1);
        metadata.push_n(CHUNK_BLOCKS, meta);
        CompressedData{ indices, metadata }
    }
    /// Transform compressed chunk data into raw data
    pub fn decompress(&self) -> RawData {
        let mut res = RawData::nulls();
        let mut indices = self.indices.iter();
        let mut metadata = self.metadata.iter();
        for data in res.flat_mut().iter_mut() {
            data.idx = *indices.next().unwrap();
            data.metadata = *metadata.next().unwrap();
        }
        res
    }
}

impl<I: Index<usize, Output=BlockIdx>, M: Index<usize, Output=TileMetadata>>
    ReadChunkData for CompressedData<I, M> {
    fn get_idx(&self, pos : InnerPos) -> BlockIdx {
        self.indices[pos.get_idx()]
    }
    fn get_meta(&self, pos : InnerPos) -> TileMetadata {
        self.metadata[pos.get_idx()]
    }
}

impl WriteChunkData for CompressedData<RleVec<BlockIdx>, RleVec<TileMetadata>> {
    fn set_idx(&mut self, pos : InnerPos, data : BlockIdx) {
        self.indices.set(pos.get_idx(), data);
    }
    fn set_meta(&mut self, pos : InnerPos, metadata : TileMetadata) {
        self.metadata.set(pos.get_idx(), metadata);
    }
}
