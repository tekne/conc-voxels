use derive_more::From;

use crate::utils::Faces;
use crate::block::{BlockIdx, TileMetadata, BlockMetadata, BlockData};
use super::pos::InnerPos;

pub mod raw;
use raw::RawData;
pub mod compressed;
use compressed::CompressedData;

pub trait ReadChunkData {
    /// Get the block data for a given position
    fn get(&self, pos : InnerPos) -> BlockData {
        BlockData {
            idx : self.get_idx(pos),
            metadata : self.get_meta(pos)
        }
    }
    /// Get the block index at a given position
    fn get_idx(&self, pos : InnerPos) -> BlockIdx;
    /// Get the block metadata at a given position
    #[inline]
    fn get_block_meta(&self, pos : InnerPos) -> BlockMetadata {
        self.get_idx(pos).get_metadata()
    }
    /// Get the metadata at a given position
    fn get_meta(&self, pos : InnerPos) -> TileMetadata;
    /// Get which faces of this block are covered (i.e. cannot be reached by light).
    fn get_covered(&self, pos : InnerPos) -> Faces {
        let mut res = Faces::empty();
        for face in Faces::all() {
            if let Some(adj) = pos.adjacent(face) {
                if self.get_block_meta(adj).opaque() { res |= face }
            }
        }
        res
    }
    /// Get which faces of this block are blocked (i.e. cannot be reached by objects).
    fn get_blocked(&self, pos : InnerPos) -> Faces {
        let mut res = Faces::empty();
        for face in Faces::all() {
            if let Some(adj) = pos.adjacent(face) {
                if self.get_block_meta(adj).solid() { res |= face }
            }
        }
        res
    }
}

pub trait WriteChunkData {
    /// Set the block data for a given position
    fn set(&mut self, pos : InnerPos, data : BlockData) {
        self.set_idx(pos, data.idx);
        self.set_meta(pos, data.metadata)
    }
    /// Set the block index for a given position
    fn set_idx(&mut self, pos : InnerPos, data : BlockIdx);
    /// Set the metadata of a block
    fn set_meta(&mut self, pos : InnerPos, metadata : TileMetadata);
}

pub trait ChunkData : ReadChunkData + WriteChunkData {}
impl<T: ReadChunkData + WriteChunkData> ChunkData for T {}

/// The blocks contained within a chunk, as well as metadata (e.g. which faces are covered)
#[derive(Debug, Clone)]
#[derive(From)]
pub enum ChunkDataEnum {
    /// Data stored as a raw array in a `Box`
    Raw(Box<RawData>),
    /// Data stored compressed as `RleVec`s
    Compressed(CompressedData)
}

impl ChunkDataEnum {
    /// Compress the data stored in this ChunkDataEnum. Note this may make the data *bigger* if
    /// the blocks within are very scrambled
    pub fn compress(&mut self) {
        use ChunkDataEnum::*;
        match self {
            Raw(r) => {
                *self = Compressed(r.compress())
            },
            Compressed(_) => {}
        }
    }
}

impl ReadChunkData for ChunkDataEnum {
    fn get(&self, pos : InnerPos) -> BlockData {
        use ChunkDataEnum::*;
        match self {
            Raw(r) => r.get(pos),
            Compressed(c) => c.get(pos),
            //Block(b) => b.get(pos)
        }
    }
    fn get_idx(&self, pos : InnerPos) -> BlockIdx {
        use ChunkDataEnum::*;
        match self {
            Raw(r) => r.get_idx(pos),
            Compressed(c) => c.get_idx(pos),
            //Block(b) => b.get_idx(pos)
        }
    }
    fn get_meta(&self, pos : InnerPos) -> TileMetadata {
        use ChunkDataEnum::*;
        match self {
            Raw(r) => r.get_meta(pos),
            Compressed(c) => c.get_meta(pos),
            //Block(b) => b.get_meta(pos)
        }
    }
}

impl WriteChunkData for ChunkDataEnum {
    fn set(&mut self, pos : InnerPos, data : BlockData) {
        use ChunkDataEnum::*;
        match self {
            Raw(r) => r.set(pos, data),
            Compressed(c) => c.set(pos, data)
        }
    }
    fn set_idx(&mut self, pos : InnerPos, data : BlockIdx) {
        use ChunkDataEnum::*;
        match self {
            Raw(r) => r.set_idx(pos, data),
            Compressed(c) => c.set_idx(pos, data)
        }
    }
    fn set_meta(&mut self, pos : InnerPos, metadata : TileMetadata) {
        use ChunkDataEnum::*;
        match self {
            Raw(r) => r.set_meta(pos, metadata),
            Compressed(c) => c.set_meta(pos, metadata)
        }
    }
}
