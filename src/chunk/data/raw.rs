use crate::block::{BlockIdx, TileMetadata};
use crate::chunk::{pos::InnerPos, CHUNK_SIZE, CHUNK_BLOCKS};
use super::compressed::CompressedData;
use super::{ReadChunkData, WriteChunkData, BlockData};

/// Raw chunk data stored as an array
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct RawData {
    /// Data associated with each block in the chunk
    pub block_data : [[[BlockData; CHUNK_SIZE]; CHUNK_SIZE]; CHUNK_SIZE]
}

impl RawData {
    /// Generate a new chunk with all blocks set to null (w/ null metadata)
    pub fn nulls() -> RawData {
        RawData {
            block_data : [[[ BlockData::null() ; CHUNK_SIZE ]; CHUNK_SIZE]; CHUNK_SIZE]
        }
    }
    /// View the data for each block as an array
    pub fn data(&self) -> &[[[BlockData; CHUNK_SIZE]; CHUNK_SIZE]; CHUNK_SIZE] {
        &self.block_data
    }
    /// View the data for each block as a mutable array
    pub fn data_mut(&mut self) -> &mut[[[BlockData; CHUNK_SIZE]; CHUNK_SIZE]; CHUNK_SIZE] {
        &mut self.block_data
    }
    /// View this block data as a flat array
    pub fn flat(&self) -> &[BlockData; CHUNK_BLOCKS] {
        unsafe {
            // Yes, I know, scary. But there is no cleaner way. Const generics, hurry up!
            std::mem::transmute(self.data())
        }
    }
    /// View this block data as a mutable flat array
    pub fn flat_mut(&mut self) -> &mut [BlockData; CHUNK_BLOCKS] {
        unsafe {
            // Yes, I know, scary. But there is no cleaner way. Const generics, hurry up!
            std::mem::transmute(self.data_mut())
        }
    }
    /// Iterate over this block data, block by block
    pub fn iter(&self) -> impl Iterator<Item=&BlockData> {
        self.flat().iter()
    }
    /// Compress this block data
    pub fn compress(&self) -> CompressedData {
        let indices = self.iter().map(|data| data.idx).collect();
        let metadata = self.iter().map(|data| data.metadata).collect();
        CompressedData::new(indices, metadata).unwrap()
    }
    /// Get the block data for a given position
    pub fn get(&self, pos : InnerPos) -> BlockData {
        self.flat()[pos.get_idx()]
    }
    /// Get the block index at a given position
    pub fn get_idx(&self, pos : InnerPos) -> BlockIdx {
        self.get(pos).idx
    }
}

impl ReadChunkData for RawData {
    fn get(&self, pos : InnerPos) -> BlockData {
        self.flat()[pos.get_idx()]
    }
    fn get_idx(&self, pos : InnerPos) -> BlockIdx {
        self.get(pos).idx
    }
    fn get_meta(&self, pos : InnerPos) -> TileMetadata {
        self.get(pos).metadata
    }
}

impl WriteChunkData for RawData {
    fn set(&mut self, pos : InnerPos, data : BlockData) {
        self.flat_mut()[pos.get_idx()] = data
    }
    fn set_idx(&mut self, pos : InnerPos, data : BlockIdx) {
        self.flat_mut()[pos.get_idx()].idx = data;
    }
    fn set_meta(&mut self, pos : InnerPos, metadata : TileMetadata) {
        self.flat_mut()[pos.get_idx()].metadata = metadata;
    }
}
