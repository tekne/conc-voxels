use nalgebra::Vector3;
use derive_more::{
    From, Index, IndexMut,
    Add, Sub, Mul, Div, Rem,
    AddAssign, SubAssign, MulAssign, DivAssign, RemAssign
};
use num::{Integer, ToPrimitive, FromPrimitive};

use crate::utils::{Face, Faces};
use super::{CHUNK_BLOCKS, CHUNK_SIZE};

/// Get the integer coordinates adjacent to this one given a direction
pub fn adjacent(coords : Vector3<i32>, face : Face) -> Vector3<i32> {
    use Face::*;
    match face {
        Top => (coords + Vector3::new(0, 1, 0)),
        Bottom => (coords + Vector3::new(0, -1, 0)),
        Left => (coords + Vector3::new(-1, 0, 0)),
        Right => (coords + Vector3::new(1, 0, 0)),
        Front => (coords + Vector3::new(0, 0, 1)),
        Back => (coords + Vector3::new(0, 0, -1))
    }
}

/// The position of an individual tile in a set of chunks
#[derive(Debug, Copy, Clone, From)]
#[derive(Eq, PartialEq)]
#[derive(Add, Sub, Mul, Div, Rem)]
#[derive(AddAssign, SubAssign, MulAssign, DivAssign, RemAssign)]
#[derive(Index, IndexMut)]
pub struct TilePos(pub Vector3<i32>);

impl TilePos {
    /// Create a new tile position given its coordinates
    pub fn new(x : i32, y : i32, z : i32) -> TilePos {
        TilePos(Vector3::new(x, y, z))
    }
    /// Factor a tile position into the chunk containing it, and the inner position within it
    pub fn factor(&self) -> (ChunkPos, InnerPos) {
        let cp = ChunkPos::containing(*self);
        let ip = self.0 - 8 * cp.0;
        (cp, InnerPos::new(ip.x, ip.y, ip.z).unwrap())
    }
    /// Get the tile position adjacent to this one in a given direction
    pub fn adjacent(&self, face : Face) -> TilePos {
        adjacent(self.0, face).into()
    }
}

/// The position of an individual chunk in a set of chunks
#[derive(Debug, Copy, Clone, From)]
#[derive(Eq, PartialEq)]
#[derive(Add, Sub, Mul, Div, Rem)]
#[derive(AddAssign, SubAssign, MulAssign, DivAssign, RemAssign)]
#[derive(Index, IndexMut)]
pub struct ChunkPos(pub Vector3<i32>);

impl ChunkPos {
    /// Create a new chunk position given its coordinates
    pub fn new(x : i32, y : i32, z : i32) -> ChunkPos {
        ChunkPos(Vector3::new(x, y, z))
    }
    /// Get the chunk position containing a given tile position
    pub fn containing(tile : TilePos) -> ChunkPos {
        ChunkPos::new(tile[0].div_floor(&8), tile[1].div_floor(&8), tile[2].div_floor(&8))
    }
    /// Get the chunk position adjacent to this one in a given direction
    pub fn adjacent(&self, face : Face) -> ChunkPos {
        adjacent(self.0, face).into()
    }
}

/// An iterator over all possible inner positions in a chunk
pub struct InnerPosIterator {
    curr : u16
}

impl InnerPosIterator {
    pub fn start() -> InnerPosIterator {
        InnerPosIterator{ curr : 0 }
    }
    pub fn at(pos : InnerPos) -> InnerPosIterator {
        InnerPosIterator{ curr : pos.get_idx() as u16 }
    }
    pub fn curr(&self) -> Option<InnerPos> {
        InnerPos::from_idx(self.curr)
    }
}

impl Iterator for InnerPosIterator {
    type Item = InnerPos;
    fn next(&mut self) -> Option<InnerPos> {
        let res = self.curr();
        self.curr += 1;
        res
    }
}

impl ExactSizeIterator for InnerPosIterator {
    fn len(&self) -> usize {
        let curr = self.curr as usize;
        if curr < CHUNK_BLOCKS { CHUNK_BLOCKS - curr } else { 0 }
    }
}

/// An inner position within a single chunk of blocks
#[derive(Debug, Copy, Clone)]
#[derive(Eq, PartialEq, Ord, PartialOrd)]
pub struct InnerPos { pos : u16 }

impl InnerPos {
    /// Attempt to create a new inner position given x, y and z coordinates. Return `None` on fail.
    pub fn new<X: ToPrimitive, Y: ToPrimitive, Z: ToPrimitive>(x : X, y : Y, z : Z)
    -> Option<InnerPos> {
        let x = x.to_u16()?;
        let y = y.to_u16()?;
        let z = z.to_u16()?;
        let chunk_size = CHUNK_SIZE as u16;
        if x >= chunk_size || y >= chunk_size || z >= chunk_size {
            None
        } else {
            let pos = x + y * chunk_size + z * chunk_size * chunk_size;
            Some(InnerPos{ pos : pos as u16 })
        }
    }
    /// Attempt to create a new inner position given an index. Return `None` on fail.
    pub fn from_idx<I: ToPrimitive>(idx : I) -> Option<InnerPos> {
        let idx = idx.to_u16()?;
        if idx < CHUNK_BLOCKS as u16 {
            Some(InnerPos{ pos : idx })
        } else {
            None
        }
    }
    /// Get an iterator of all possible inner positions in a chunk
    pub fn all() -> InnerPosIterator {
        InnerPosIterator::start()
    }
    /// Get the index into a chunk array associated with this inner position
    pub fn get_idx(&self) -> usize { self.pos as usize }
    /// Get the x-coordinate in the chunk of this inner position
    #[inline]
    pub fn x<T: FromPrimitive>(&self) -> T {
        T::from_u16(self.pos % CHUNK_SIZE as u16).unwrap()
    }
    /// Get the y-coordinate in the chunk of this inner position
    #[inline]
    pub fn y<T: FromPrimitive>(&self) -> T {
        let filter = self.pos % (CHUNK_SIZE * CHUNK_SIZE) as u16;
        T::from_u16(filter / CHUNK_SIZE as u16).unwrap()
    }
    /// Get the z-coordinate in the chunk of this inner position
    #[inline]
    pub fn z<T: FromPrimitive>(&self) -> T {
        T::from_u16(self.pos / (CHUNK_SIZE * CHUNK_SIZE) as u16).unwrap()
    }
    /// Get the adjacent inner position corresponding to a given face, if a valid one exists.
    pub fn adjacent(&self, face : Face) -> Option<InnerPos> {
        use Face::*;
        let (x, y, z) : (i32, i32, i32) = (self.x(), self.y(), self.z());
        match face {
            Top => Self::new(x, y + 1, z),
            Bottom => Self::new(x, y - 1, z),
            Left => Self::new(x - 1, y, z),
            Right => Self::new(x + 1, y, z),
            Front => Self::new(x, y, z + 1),
            Back => Self::new(x, y, z - 1)
        }
    }
    /// Get which faces of this position's cube, if any, are chunk edges
    pub fn edges(&self) -> Faces {
        use Face::*;
        let mut result = Faces::empty();
        let (x, y, z) : (usize, usize, usize) = (self.x(), self.y(), self.z());
        if x == 0 {
            result |= Left;
        } else if x == CHUNK_SIZE - 1 {
            result |= Right;
        }
        if y == 0 {
            result |= Bottom;
        } else if y == CHUNK_SIZE - 1 {
            result |= Top;
        }
        if z == 0 {
            result |= Back;
        } else if z == CHUNK_SIZE - 1 {
            result |= Front;
        }
        result
    }
    /// Get the faces corresponding to *valid* blocks behind this position in forward iteration
    #[inline]
    pub fn behind(&self) -> Faces {
        use Face::*;
        let mut result = Faces::empty();
        let (x, y, z) : (usize, usize, usize) = (self.x(), self.y(), self.z());
        if x != 0 {
            result |= Right;
        }
        if y != 0 {
            result |= Bottom;
        }
        if z != 0 {
            result |= Back;
        }
        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn simple_factoring_works() {
        let chunk_size = CHUNK_SIZE as i32;
        let pos = TilePos::new(3 + 4 * chunk_size,  2 + 2 * chunk_size, 7 + 8 * chunk_size);
        let (chunk_pos, inner_pos) = pos.factor();
        assert_eq!(chunk_pos, ChunkPos::new(4, 2, 8));
        assert_eq!(inner_pos, InnerPos::new(3, 2, 7).unwrap());
    }

    #[test]
    fn negative_factoring_works() {
        let chunk_size = CHUNK_SIZE as i32;
        let pos = TilePos::new(3 + 4 * chunk_size,  2 - 2 * chunk_size, 1 - 8 * chunk_size);
        let (chunk_pos, inner_pos) = pos.factor();
        assert_eq!(chunk_pos, ChunkPos::new(4, -2, -8));
        assert_eq!(inner_pos, InnerPos::new(3, 2, 1).unwrap());
    }

    #[test]
    fn adjacent_inner_pos_works() {
        use crate::utils::Face::*;
        let inner = InnerPos::new(CHUNK_SIZE - 1, 0, 2).unwrap();
        assert_eq!(inner.adjacent(Left).unwrap(), InnerPos::new(CHUNK_SIZE - 2, 0, 2).unwrap());
        assert_eq!(inner.adjacent(Right), None);
        assert_eq!(inner.adjacent(Bottom), None);
        assert_eq!(inner.adjacent(Top).unwrap(), InnerPos::new(CHUNK_SIZE - 1, 1, 2).unwrap());
        assert_eq!(inner.adjacent(Back).unwrap(), InnerPos::new(CHUNK_SIZE - 1, 0, 1).unwrap());
        assert_eq!(inner.adjacent(Front).unwrap(), InnerPos::new(CHUNK_SIZE - 1, 0, 3).unwrap());
    }

}
