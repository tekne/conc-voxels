use std::sync::atomic::{AtomicUsize, Ordering};

/// An ID representing an entity in the voxel world
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
pub struct EntityID {
    /// The underlying integer ID
    pub(self) num : usize
}

impl<T: Entity> From<TypedEntityID<T>> for EntityID {
    /// Extract the underlying untyped ID from a typed ID
    fn from(teid : TypedEntityID<T>) -> EntityID {
        teid.eid
    }
}

/// An ID representing an entity of a specific type in the voxel world
#[derive(Debug, Copy, Clone)]
#[derive(Hash, Eq, PartialEq, Ord, PartialOrd)]
pub struct TypedEntityID<T: Entity> {
    /// The underlying entity ID
    pub(self) eid : EntityID,
    pub(self) _phantom : std::marker::PhantomData<fn() -> T>
}

/// A factory for entity IDs, returning a unique typed ID for each call to `register_entity`
#[derive(Debug)]
pub struct EntityFactory {
    num_entities : AtomicUsize
}

impl EntityFactory {
    pub fn new() -> EntityFactory {
        EntityFactory{ num_entities : AtomicUsize::new(0) }
    }
    pub fn register_entity<T: Entity>(&self, _entity : &T) -> TypedEntityID<T> {
        TypedEntityID {
            eid : EntityID { num : self.num_entities.fetch_add(1, Ordering::Relaxed) },
            _phantom : std::marker::PhantomData
        }
    }
}

/// An entity in the voxel world
pub trait Entity {
}

/// An enum of the most common types of entities in the voxel world for efficient storage
pub enum EntityEnum {
    Chunks(Chunks),
    World(World),
    Tile(Tile),
    Mob(Mob),
    Dyn(Box<dyn Entity>)
}

impl Entity for EntityEnum {}

/// An entity composed of a small number of chunks of blocks
#[derive(Debug, Clone)]
pub struct Chunks {

}

impl Entity for Chunks {}

/// An entity composed of a large number of chunks of blocks with an associated world-generator.
#[derive(Debug, Clone)]
pub struct World {

}

impl Entity for World {}

/// An entity which lives inside a set of chunks (whether in a `World` or in a `Chunks`).
#[derive(Debug, Clone)]
pub struct Tile {

}

impl Entity for Tile {}

/// An entity not composed of blocks, e.g. the player
#[derive(Debug, Clone)]
pub struct Mob {

}

impl Entity for Mob {

}
