use enumset::{EnumSet, EnumSetType};

/// The faces of a cube
#[derive(Debug, EnumSetType)]
pub enum Face {
    Top,
    Bottom,
    Left,
    Right,
    Front,
    Back
}

impl Face {
    /// Get the face opposite to a given face
    pub fn flip(&self) -> Face {
        use Face::*;
        match self {
            Top => Bottom,
            Bottom => Top,
            Left => Right,
            Right => Left,
            Front => Back,
            Back => Front
        }
    }
}

pub type Faces = EnumSet<Face>;
